package com.assignment;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class testSteps {

    ElementSymbols es;
    private String elementName;
    boolean elementSymbolValid;
    int givenNumberOfCombinations;

    @Given("^I have an element \"([^\"]*)\"$")
    public void I_have_an_element(String arg1) throws Throwable {

        System.out.println("steps");

        es = new ElementSymbols();
        es.setName(arg1);
        this.elementName = arg1;

        es.findAlltheCombinations(elementName);
    }

    @When("^passed \"([^\"]*)\" as its symbol$")
    public void passed_as_its_symbol(String arg1) throws Throwable {

        //es.findAlltheCombinations(elementName);
        elementSymbolValid = es.uniqueElementSymbols.contains(arg1.toLowerCase());

    }

    @Then("^I should be told that it is a right symbol$")
    public void I_should_be_told_that_it_is_a_right_symbol() throws Throwable {

        boolean expectedResult = true;

        if(elementSymbolValid == true) {

            System.out.println("This is a valid symbol");

        }

        else {

            System.out.println("This is not a valid symbol");
        }

        assertEquals(expectedResult, elementSymbolValid);

    }


    @Then("^I should be told that it is a wrong symbol$")
    public void I_should_be_told_that_it_is_a_wrong_symbol() throws Throwable {

        boolean expectedResult = false;

        if(elementSymbolValid == true) {

            System.out.println("This is a valid symbol");

        }

        else {

            System.out.println("This is not a valid symbol");
        }

        assertEquals(expectedResult, elementSymbolValid);
    }

    @When("^I say the possible combinations is (\\d+)$")
    public void I_say_the_possible_combinations_is(int arg1) throws Throwable {

        givenNumberOfCombinations = arg1;

    }

    @Then("^I should be told that it is right$")
    public void I_should_be_told_that_it_is_right() throws Throwable {

        assertEquals(givenNumberOfCombinations, es.numberOfPossibleCombinations);

    }

    @Then("^I should be told that it is wrong$")
    public void I_should_be_told_that_it_is_wrong() throws Throwable {

        assertNotEquals(givenNumberOfCombinations, es.numberOfPossibleCombinations);

    }

    @Then("^I should be told that it is a wrong element name$")
    public void I_should_be_told_that_it_is_a_wrong_element_name() throws Throwable {
        if (es.uniqueElementSymbols.isEmpty()) {
        }
    }


}
