/** This file contains the actual implementation of the element names, symbols and possible combinations for it.
 * This contains certain requirements as
 * A. Order of the element symbols
 * B. No special characters
 * C. Handle repetitions
 * D.Have only 2 characters as the element symbol
 * E. Minimum of 2 characters as an element name
 * F.Case-insensitive
 *
 */


package com.assignment;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ElementSymbols {

    static List<String> listOfElementSymbol = new ArrayList<String>();
    static Set<String> uniqueElementSymbols;
    static int numberOfPossibleCombinations;
    static String elementName;

    public void setName(String name)
    {
        this.elementName = name;
    }

    public static void main (String[] args) {

        //findAlltheCombinations(elementName);

    }


    public static void findAlltheCombinations(String element) {

        String tempElementSymbol;
        //This is to check for the empty spaces
        element = element.trim();

        Pattern p = Pattern.compile("[^A-Za-z]");
        Matcher m = p.matcher(element);
        // boolean b = m.matches();
        boolean b = m.find();

        if (element == null || element.isEmpty()) {

            System.out.println("Can't pass an empty element name");

        } else if (element.length() < 2) {

            System.out.println("There should be a minimum of 2 characters in the string");
        } else if (b == true) {

            System.out.println("There is a special character in your string");

        } else {

            for (int i = 0; i < element.length(); i++) {

                for (int j = i + 1; j < element.length(); j++) {

                    tempElementSymbol = String.valueOf(element.charAt(i)).toLowerCase() + String.valueOf(element.charAt(j)).toLowerCase();

                    listOfElementSymbol.add(tempElementSymbol);

                }
            }

            uniqueElementSymbols = new HashSet<String>(listOfElementSymbol);

            //Make them distinct
            numberOfPossibleCombinations = uniqueElementSymbols.size();

            System.out.println(listOfElementSymbol);
            System.out.println(uniqueElementSymbols);
            System.out.println(numberOfPossibleCombinations);

        }

    }

}
