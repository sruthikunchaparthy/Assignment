Feature: This is to write tests for the element names

Scenario: Check a right symbol against the element name

    Given I have an element "Zuulon"
    When passed "Zn" as its symbol
    Then I should be told that it is a right symbol

Scenario: Check a wrong symbol with 2 characters against the element name

    Given I have an element "Zuulon"
    When passed "ZZ" as its symbol
    Then I should be told that it is a wrong symbol


Scenario: Check a wrong symbol with more than 2 characters against the element name

    Given I have an element "Zuulon"
    When passed "ZZZZZZZ" as its symbol
    Then I should be told that it is a wrong symbol


Scenario: Check a wrong symbol with a special character against the element name

    Given I have an element "Zuulon"
    When passed "Z#" as its symbol
    Then I should be told that it is a wrong symbol

Scenario: Check a wrong symbol with a number against the element name

    Given I have an element "Zuulon"
    When passed "Z#" as its symbol
    Then I should be told that it is a wrong symbol

Scenario: Check with the right number of combinations

    Given I have an element "Zuulon"
    When I say the possible combinations is 11
    Then I should be told that it is right

Scenario: Check with the wrong number of combinations

    Given I have an element "Zuulon"
    When I say the possible combinations is 111
    Then I should be told that it is wrong

Scenario: Check a symbol with wrong order

    Given I have an element "Zuulon"
    When passed "uz" as its symbol
    Then I should be told that it is a wrong symbol

Scenario: Check an element with less than 2 characters

    Given I have an element "Z"
    When passed "uz" as its symbol
    Then I should be told that it is a wrong element name